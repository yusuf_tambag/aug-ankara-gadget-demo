package tr.atlassian.aug.ankara.jira.gadget.util;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Collection;

@XmlRootElement
public class ErrorCollection {
	@XmlElement
	private Collection<String> errorMessages=new ArrayList<String>();

	@XmlElement
	private Collection<ValidationError> errors=new ArrayList<ValidationError>();

	public Collection<String> getErrorMessages() {
		return errorMessages;
	}

	public Collection<ValidationError> getErrors() {
		return errors;
	}

	public void addErrorMessage(String errorMessage) {
		errorMessages.add(errorMessage);
	}

	public void addValidationError(ValidationError validationError) {
		errors.add(validationError);
	}

	public boolean hasAnyErrors() {
		return errorMessages.size()>0 || errors.size()>0;
	}
}
