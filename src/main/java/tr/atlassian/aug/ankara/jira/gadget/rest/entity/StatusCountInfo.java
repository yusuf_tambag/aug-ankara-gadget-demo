package tr.atlassian.aug.ankara.jira.gadget.rest.entity;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class StatusCountInfo {
    @XmlAttribute
    private String status;
    @XmlAttribute
    private int count=0;

    public StatusCountInfo() {

    }

    public StatusCountInfo(String status,int count) {
        this.status = status;
        this.count = count;
    }

    public String getStatus() {
        return status;
    }

    public int getCount() {
        return count;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
