package tr.atlassian.aug.ankara.jira.gadget.rest.entity;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class UserIssueStatus {
    @XmlAttribute
    private String userName;
    @XmlElement
    private List<StatusCountInfo> statusInfoList;

    public UserIssueStatus() {

    }

    public UserIssueStatus(String userName) {
        this.userName = userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public List<StatusCountInfo> getStatusInfoList() {
        return statusInfoList;
    }

    public void addStatusCountInfo(StatusCountInfo countInfo) {
        if(statusInfoList==null)
            statusInfoList = new ArrayList<StatusCountInfo>();
        statusInfoList.add(countInfo);
    }
}
