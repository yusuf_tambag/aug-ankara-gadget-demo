package tr.atlassian.aug.ankara.jira.gadget.util;

import sun.misc.BASE64Encoder;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ChartUtils {
	public static String createChartFile(String chartName) {
		return chartName+"-"+(new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime()))+".png";
	}

	public static String getFileURL(HttpServletRequest request,String fileURL) {
		try {
			request.getSession(true).getServletContext().getContextPath();
			return URLEncoder.encode(fileURL,"UTF-8");
		} catch(UnsupportedEncodingException ex) {
			ex.printStackTrace();
			return fileURL;
		}
	}

	public static String encodeToString(BufferedImage image, String type) {
		String imageString = null;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		try {
			ImageIO.write(image, type, bos);
			byte[] imageBytes = bos.toByteArray();

			BASE64Encoder encoder = new BASE64Encoder();
			imageString = encoder.encode(imageBytes);

			bos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return imageString;
	}

}
