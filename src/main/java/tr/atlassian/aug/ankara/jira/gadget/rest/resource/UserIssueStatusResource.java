package tr.atlassian.aug.ankara.jira.gadget.rest.resource;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.imagemap.ImageMapUtilities;
import org.jfree.chart.imagemap.StandardToolTipTagFragmentGenerator;
import org.jfree.chart.imagemap.StandardURLTagFragmentGenerator;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.urls.StandardCategoryURLGenerator;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.TextAnchor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tr.atlassian.aug.ankara.jira.gadget.rest.entity.StatusCountInfo;
import tr.atlassian.aug.ankara.jira.gadget.rest.entity.UserIssueStatus;
import tr.atlassian.aug.ankara.jira.gadget.rest.entity.UserStatusList;
import tr.atlassian.aug.ankara.jira.gadget.util.ChartUtils;
import tr.atlassian.aug.ankara.jira.gadget.util.RandomString;
import tr.atlassian.aug.ankara.jira.gadget.util.ValidationError;
import tr.atlassian.aug.ankara.jira.gadget.util.ValidationErrorList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

@Path("/user-issue-status")
public class UserIssueStatusResource {
    private static final Logger LOG=LoggerFactory.getLogger(UserIssueStatusResource.class);
    private static final String PROJECT_STRING="project";
    private static final String PROJECT_ID="Project ID";

    @GET
    @Path("assignment-status")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAssignmentStatus(@CookieParam("JSESSIONID") String sessionId, @QueryParam("projectId") String projectId) {
        LOG.debug("Project ID: "+projectId);
        JiraAuthenticationContext authenticationContext =ComponentAccessor.getJiraAuthenticationContext();
        ApplicationUser loggedInUser=authenticationContext.getLoggedInUser();
        if(loggedInUser==null)
            return Response.status(Response.Status.UNAUTHORIZED).build();
        ValidationErrorList validationErrorList=new ValidationErrorList();
        List<Issue> issueList=getProjectIssues(loggedInUser,projectId,validationErrorList);
        if(!validationErrorList.errorOccured()) {
            UserStatusList statusList=buildStatusList(issueList,ReturnTypeEnum.ASSIGNEE,validationErrorList);
            if(!validationErrorList.errorOccured() && statusList!=null) {
                return Response.status(Response.Status.OK).entity(statusList).build();
            } else if(validationErrorList.errorOccured())
                return Response.status(Response.Status.BAD_REQUEST).entity(validationErrorList).build();
            else
                return Response.status(Response.Status.NO_CONTENT).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).entity(validationErrorList).build();
        }
    }

    @GET
    @Path("reporter-status")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getReporterStatus(@CookieParam("JSESSIONID") String sessionId,@QueryParam("projectId") String projectId) {
        JiraAuthenticationContext authenticationContext =ComponentAccessor.getJiraAuthenticationContext();
        ApplicationUser loggedInUser=authenticationContext.getLoggedInUser();
        if(loggedInUser==null)
            return Response.status(Response.Status.UNAUTHORIZED).build();
        ValidationErrorList validationErrorList=new ValidationErrorList();
        List<Issue> issueList=getProjectIssues(loggedInUser,projectId,validationErrorList);
        if(!validationErrorList.errorOccured()) {
            UserStatusList statusList=buildStatusList(issueList,ReturnTypeEnum.REPORTER,validationErrorList);
            if(!validationErrorList.errorOccured() && statusList!=null) {
                return Response.status(Response.Status.OK).entity(statusList).build();
            } else if(validationErrorList.errorOccured())
                return Response.status(Response.Status.BAD_REQUEST).entity(validationErrorList).build();
            else
                return Response.status(Response.Status.NO_CONTENT).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).entity(validationErrorList).build();
        }
    }

    public List<Issue> getProjectIssues(ApplicationUser loggedInUser,String projectId, ValidationErrorList validationErrorList) {
        int index=projectId.indexOf(PROJECT_STRING+"-");
        if(index==-1) {
            validationErrorList.addValidationError(new ValidationError(PROJECT_ID,"Invalid project id format '"+projectId+"'"));
            return null;
        }

        Long id=-1l;
        try {
            id=Long.parseLong(projectId.substring(index + (PROJECT_STRING.length() + 1)));
            ProjectManager projectManager = ComponentAccessor.getProjectManager();
            Project project=projectManager.getProjectObj(id);
            if(project==null) {
                validationErrorList.addValidationError(new ValidationError(PROJECT_ID, "Invalid project id format '" + projectId + "'"));
                return null;
            } else {
                List<Issue> issueList=new ArrayList<Issue>();
                SearchService searchService=ComponentAccessor.getComponentOfType(SearchService.class);
                String jqlString="project="+project.getKey();
                SearchService.ParseResult parseResult=searchService.parseQuery(loggedInUser,jqlString);
                if(!parseResult.isValid()) {
                    validationErrorList.addValidationError(new ValidationError(PROJECT_STRING,"Unable to parse query: "+jqlString));
                } else {
                    Query query=parseResult.getQuery();
                    try {
                        SearchResults searchResults=searchService.search(loggedInUser,query, PagerFilter.getUnlimitedFilter());
                        issueList=searchResults.getIssues();
                    } catch (SearchException e) {
                        validationErrorList.addValidationError(new ValidationError("Query","Search error occured:"+e.getMessage()));
                    }
                }
                if(validationErrorList.errorOccured())
                    return null;
                return issueList;
            }
        } catch (NumberFormatException ex) {
            validationErrorList.addValidationError(new ValidationError(PROJECT_ID,"Invalid project id format '"+projectId+"'"));
            return null;
        }
    }

    private UserStatusList buildStatusList(List<Issue> issueList,ReturnTypeEnum returnTypeEnum,ValidationErrorList validationErrorList) {
        UserStatusList statusList = new UserStatusList();
        HashMap<String,UserIssueStatus> userHashMap=new HashMap<String, UserIssueStatus>();
        String projectKey=null;
        int height=400;
        List<String> userNameList=new ArrayList<String>();
        for(Issue issue:issueList) {
            String status=issue.getStatus().getName();
            statusList.addStatus(status);
            if(projectKey==null)
                projectKey = issue.getProjectObject().getKey();
            ApplicationUser applicationUser=(returnTypeEnum==ReturnTypeEnum.ASSIGNEE ? issue.getAssignee() : issue.getReporter());
            if(applicationUser==null)
                continue;
            String userName=applicationUser.getName();
            if(!userNameList.contains(userName))
                userNameList.add(userName);
            UserIssueStatus issueStatus=userHashMap.get(userName);
            if(issueStatus==null) {
                issueStatus=new UserIssueStatus(userName);
                userHashMap.put(userName,issueStatus);
            }
            increaseStatusCountInfo(status,issueStatus);
        }
        int width=50*userNameList.size()+250;
        int size=userHashMap.size();
        if(size>0) {
            final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
            Set<String> keySet=userHashMap.keySet();
            for(String key:keySet) {
                UserIssueStatus issueStatus = userHashMap.get(key);
                statusList.addUserIssueStatus(issueStatus);
                List<StatusCountInfo> countInfoList=issueStatus.getStatusInfoList();
                if(countInfoList!=null) {
                    for(StatusCountInfo countInfo:countInfoList)
                        dataset.addValue(countInfo.getCount(),countInfo.getStatus(),key);
                }
            }
            JFreeChart barChart = ChartFactory.createBarChart((returnTypeEnum == ReturnTypeEnum.ASSIGNEE ? "Assignee" : "Reporter") + " Issue Count", "User Name",
                    "Issue Count", dataset, PlotOrientation.VERTICAL, true, true, true);
            CategoryPlot categoryPlot=barChart.getCategoryPlot();
            CategoryAxis domainAxis=categoryPlot.getDomainAxis();
            ValueAxis valueAxis = categoryPlot.getRangeAxis();
            valueAxis.setAutoRange(true);
            valueAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
            domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_90);
            domainAxis.setCategoryLabelPositionOffset(10);
            String chartName="chart-" + new RandomString(10).nextString();

            CategoryItemRenderer renderer = categoryPlot.getRenderer();
            renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
            renderer.setBaseItemLabelsVisible(true);
            renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator());
            ItemLabelPosition position = new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12,
                    TextAnchor.TOP_CENTER);
            renderer.setBasePositiveItemLabelPosition(position);

            AugDemoURLGenerator urlGenerator=new AugDemoURLGenerator(" ",projectKey,returnTypeEnum);
            renderer.setBaseItemURLGenerator(urlGenerator);

            ChartRenderingInfo renderingInfo = new ChartRenderingInfo(new StandardEntityCollection());
            statusList.setImageMapName(chartName);
            BufferedImage bufferedImage=barChart.createBufferedImage(width,height,renderingInfo);

            String map= ImageMapUtilities.getImageMap(chartName,renderingInfo,new StandardToolTipTagFragmentGenerator(),new StandardURLTagFragmentGenerator());
            statusList.setImageMap(map);
            statusList.setBase64Image(ChartUtils.encodeToString(bufferedImage,"png"));
            statusList.setWidth(width);
            statusList.setHeight(height+50);

            statusList.setImageMap(ImageMapUtilities.getImageMap(chartName,renderingInfo));
        }
        return statusList;
    }

    private StatusCountInfo increaseStatusCountInfo(String status, UserIssueStatus userIssueStatus) {
        List<StatusCountInfo> statusCountInfoList=userIssueStatus.getStatusInfoList();
        StatusCountInfo statusCountInfo=null;
        if(statusCountInfoList!=null) {
            for(StatusCountInfo countInfo:statusCountInfoList)
                if(countInfo.getStatus().equals(status))
                    statusCountInfo = countInfo;
        }
        if(statusCountInfo==null) {
            statusCountInfo = new StatusCountInfo(status,0);
            userIssueStatus.addStatusCountInfo(statusCountInfo);
        }
        statusCountInfo.setCount(statusCountInfo.getCount()+1);
        return statusCountInfo;
    }

    private String getJiraBaseURL() {
        return ComponentAccessor.getApplicationProperties().getString(APKeys.JIRA_BASEURL);
    }

    private enum ReturnTypeEnum {
        ASSIGNEE,
        REPORTER;
    }

    private class AugDemoURLGenerator extends StandardCategoryURLGenerator {
        private String projectKey;
        private ReturnTypeEnum returnTypeEnum;

        public AugDemoURLGenerator(String prefix,String projectKey,ReturnTypeEnum returnTypeEnum) {
            super(prefix);
            this.projectKey = projectKey;
            this.returnTypeEnum = returnTypeEnum;
        }

        public String generateURL(CategoryDataset categoryDataset, int seriesIndex, int categoryIndex) {
            String statusKey=String.class.cast(categoryDataset.getRowKey(seriesIndex));
            LOG.debug("Status Key: "+statusKey);
            String userKey=String.class.cast(categoryDataset.getColumnKey(categoryIndex));

            if(statusKey==null || userKey==null) // Must not enter here..
                return getJiraBaseURL();
            return getJiraBaseURL()+"/issues/?jql=project %3D "+projectKey+" AND "+(returnTypeEnum.equals(ReturnTypeEnum.ASSIGNEE) ? "assignee" : "reporter")+
                    " %3D "+userKey+" AND status %3D &quot;"+statusKey+"&quot;";
        }
    }
}
