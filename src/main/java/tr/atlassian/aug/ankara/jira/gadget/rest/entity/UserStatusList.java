package tr.atlassian.aug.ankara.jira.gadget.rest.entity;

import jdk.nashorn.internal.ir.annotations.Immutable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Collection;

@XmlRootElement
public class UserStatusList {
    @XmlElement
    private Collection<UserIssueStatus> userIssueStatusList=null;
    @XmlElement
    private Collection<String> statusList=null;
    @XmlElement
    private String imageMapName;
    @XmlElement
    private String imageMap;
    @XmlElement
    private String base64Image;
    @XmlElement
    private int width;
    @XmlElement
    private int height;

    public UserStatusList() {

    }

    public void addUserIssueStatus(UserIssueStatus userIssueStatus) {
        if(userIssueStatusList==null)
            userIssueStatusList = new ArrayList<UserIssueStatus>();
        userIssueStatusList.add(userIssueStatus);
    }

    public Collection<UserIssueStatus> getUserIssueStatusList() {
        return userIssueStatusList;
    }

    public void setImageMapName(String imageMapName) {
        this.imageMapName = imageMapName;
    }

    public void setImageMap(String imageMap) {
        this.imageMap = imageMap;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }

    public void addStatus(String status) {
        if(statusList==null)
            statusList=new ArrayList<String>();
        if(!statusList.contains(status))
            statusList.add(status);
    }
    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public String getBase64Image() {
        return base64Image;
    }

    public String getImageMap() {
        return imageMap;
    }

    public String getImageMapName() {
        return imageMapName;
    }

    public Collection<String> getStatusList() {
        return statusList;
    }
}
