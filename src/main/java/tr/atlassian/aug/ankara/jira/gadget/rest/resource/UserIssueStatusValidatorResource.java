package tr.atlassian.aug.ankara.jira.gadget.rest.resource;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path( "/user-issue-status/config-validate")
public class UserIssueStatusValidatorResource {
    private static final String PROJECT_STRING="project";
    private final Logger LOG=LoggerFactory.getLogger(UserIssueStatusValidatorResource.class);

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response validate(@QueryParam("projectId") String projectId) {
        LOG.debug("Project ID: "+projectId);
        if(projectId==null || projectId.trim().equals(""))
            return Response.status(Response.Status.BAD_REQUEST).build();
        JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        ApplicationUser loggedInUser=authenticationContext.getLoggedInUser();
        if(loggedInUser==null)
            return Response.status(Response.Status.UNAUTHORIZED).build();
        int index=projectId.indexOf(PROJECT_STRING+"-");
        if(index==-1) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Invalid project id format '"+projectId+"'").build();
        }

        Long id=-1l;
        try {
            id = Long.parseLong(projectId.substring(index + (PROJECT_STRING.length() + 1)));
            ProjectManager projectManager = ComponentAccessor.getProjectManager();
            Project project = projectManager.getProjectObj(id);
            if (project == null) {
                return Response.status(Response.Status.BAD_REQUEST).entity("Invalid project id format '"+projectId+"'").build();
            } else {
                return Response.status(Response.Status.OK).build();
            }
        } catch (NumberFormatException ex) {
            LOG.error("Number format error: ",ex);
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
