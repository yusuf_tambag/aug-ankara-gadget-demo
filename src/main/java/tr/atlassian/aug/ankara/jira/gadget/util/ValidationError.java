package tr.atlassian.aug.ankara.jira.gadget.util;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

@XmlRootElement
public class ValidationError {
	@XmlElement
	private String field;
	@XmlElement
	private String error;

	@XmlElement
	private Collection<String> params;

	public ValidationError(String field, String error) {
		this.field=field;
		this.error= error;
	}

	public String getField() {
		return field;
	}

	public String getError() {
		return error;
	}

	public void setParams(Collection<String> params) {
		this.params = params;
	}

	public Collection<String> getParams() {
		return params;
	}
}
