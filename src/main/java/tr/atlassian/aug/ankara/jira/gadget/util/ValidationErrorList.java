package tr.atlassian.aug.ankara.jira.gadget.util;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.Collection;

@XmlRootElement
public class ValidationErrorList {
    @XmlElement
    private Collection<ValidationError> errorList;

    public void addValidationError(ValidationError validationError) {
        if(errorList==null)
            errorList = new ArrayList<ValidationError>();
        errorList.add(validationError);
    }

    public Collection<ValidationError> getErrorList() {
        return errorList;
    }

    @XmlTransient
    public boolean errorOccured() {
        return errorList!=null && errorList.size()>0;
    }
}
